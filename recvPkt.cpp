#include <stdio.h>
#include <time.h>
#include <pcap.h>
#include <netinet/in.h>
#include <netinet/if_ether.h>
#include <time.h>
#include <math.h>
#include <iostream>
#include <unistd.h>
#include <signal.h>
#include <chrono>
#include <iomanip>
#include <fstream>
#include <iostream>
#include <string.h>
#include <stdio.h>
#include <vector>
#include <thread>
#include <cstring>


// void print_current_time_with_ms(void);



std::chrono::high_resolution_clock::time_point ts;
std::ofstream fout;
std::string fileName = "out.txt";
uint64_t capLimit = 1000000;


std::vector<int64_t> timestamps(capLimit, 0);
std::vector<int> lengths(capLimit, 0);

std::vector<pcap_pkthdr> packetHeader(capLimit);
//std::vector<u_char> packetData(capLimit, 0);

//u_char packetData2[100000][9000];


u_char * packetData2 = new u_char[1000*9000UL]();


//struct counters{
//    int recv_count = 0;
//};



void my_packet_handler(
    u_char *args,
    const struct pcap_pkthdr *header,
    const u_char *packet);

typedef struct {
} Configuration;

uint64_t recv_count;

void task1(pcap_t *p, pcap_stat &ps)
{

    int recv_count_prev = -1;
    while (1){
        printf("Captured %d packets\n", recv_count);

        /*
            * Get the packet capture statistics associated with this packet
            * capture device. The values represent packet statistics from the time
            * pcap_open_live() was called up until this call.
            */
        if (pcap_stats(p, &ps) != 0) {
                fprintf(stderr, "Error getting Packet Capture stats: %s\n",
                        pcap_geterr(p));
                exit(10);
        }

        /* Print the statistics out */
        printf("Packet Capture Statistics:\n");
        printf("%d packets received by filter\n", ps.ps_recv);
        printf("%d packets dropped by kernel\n", ps.ps_drop);
        printf("%d packets dropped by interface\n", ps.ps_ifdrop);


        if (recv_count == recv_count_prev)
        {
            for (int i = 0; i < recv_count; i++)
            {

                // Look for data corruption
                for (int j = 12; j < 8192; j++)
                {
                    if (packetData2[i*9000UL + j] != 0x08)
                    {
                        printf("Data corruption at packet %d, byte %d\n", i, j);
                        return;
                    }
                }

            }
        }

        recv_count_prev = recv_count;
        sleep(4);
    }

}


int main(int argc, char *argv[])
{


    pcap_t *p;               /* packet capture descriptor */
    struct pcap_stat ps;     /* packet statistics */
    pcap_dumper_t *pd;       /* pointer to the dump file */
    char errbuf[PCAP_ERRBUF_SIZE];  /* buffer to hold error text */
    char lhost[256];         /* local host name */
    char fltstr[120];        /* bpf filter string */
    char prestr[80];         /* prefix string for errors from pcap_perror */
    struct bpf_program prog; /* compiled bpf filter program */
    int optimize = 1;        /* passed to pcap_compile to do optimization */
    int snaplen = 9000;      /* amount of data per packet */
    int promisc = 1;         /* do not change mode; if in promiscuous */
                                /* mode, stay in it, otherwise, do not */
    int to_ms = 1000;        /* timeout, in milliseconds */
    int count = -1;          /* number of packets to capture */
    u_int32_t net = 0;         /* network IP address */
    u_int32_t mask = 0;        /* network address mask */
    char netstr[INET_ADDRSTRLEN];   /* dotted decimal form of address */
    char maskstr[INET_ADDRSTRLEN];  /* dotted decimal form of net mask */
    int linktype = 0;        /* data link type */
    int pcount = 0;          /* number of packets actually read */




    char ifname[] = "ens7f0";
    char pcap_out[] = "./pcap_out";       /* name of savefile for dumping packet data */



    


	pcap_if_t *iface, *devs;
	if (pcap_findalldevs(&devs, errbuf) == -1 || !devs) {
		fprintf(stderr, "No network devices are currently connected\n");
		return 1;
	}
    int ii;
	printf("Enabled Network Devices:\n");
	for (ii = 0, iface = devs; iface; iface = iface->next)
		printf("%d - %s - %s - %s\n", ++ii, iface->name, iface->description, iface->flags & PCAP_IF_LOOPBACK ? "loopback" : "not loopback");






    /* Open ifname for live capture */
    if (!(p = pcap_open_live(ifname, snaplen, promisc, to_ms, errbuf))) {
            fprintf(stderr, "Error opening interface %s: %s\n",
                    ifname, errbuf);
            exit(2);
    }

    /*
     * Look up the network address and subnet mask for the network device
     * returned by pcap_lookupdev(). The network mask will be used later 
     * in the call to pcap_compile().
     */
    if (pcap_lookupnet(ifname, &net, &mask, errbuf) < 0) {
            fprintf(stderr, "Error looking up network: %s\n", errbuf);
            exit(3);
    }

    fout.open(fileName.c_str());

    fout << "Header len" << "  " << "Caplen" << "  " << "Timestamp" << "  " << "Packet" << std::endl;

    printf("Listening to Device: %s\n", ifname);


    if (pcap_compile(p,&prog,fltstr,optimize,mask) < 0) {
            /*
             * Print out appropriate text, followed by the error message
             * generated by the packet capture library.
             */
            fprintf(stderr, "Error compiling bpf filter on %s: %s\n",
                    ifname, pcap_geterr(p));
            exit(5);
    }

    if ((pd = pcap_dump_open(p,pcap_out)) == NULL) {
            /*
             * Print out error message if pcap_dump_open failed. This will
             * be the below message followed by the pcap library error text,
             * obtained by pcap_geterr().
             */
            fprintf(stderr,
                    "Error opening savefile \"%s\" for writing: %s\n",
                    pcap_out, pcap_geterr(p));
            exit(7);
    }



    std::thread t1(task1, p, std::ref(ps));

    //if ((pcount = pcap_dispatch(p, count, &pcap_dump, (u_char *)pd)) < 0) {
    //        /*
    //         * Print out appropriate text, followed by the error message
    //         * generated by the packet capture library.
    //         */
    //        sprintf(prestr,"Error reading packets from interface %s",
    //                ifname);
    //        pcap_perror(p,prestr);
    //        exit(8);
    //}
    //printf("Packets received and successfully passed through filter: %d.\n",
    //        pcount);

    //pcap_loop(p, count, &pcap_dump, (u_char *)pd);
    pcap_loop(p, count, my_packet_handler, nullptr);



    t1.join();
    


    /*
        * Get and print the link layer type for the packet capture device,
        * which is the network device selected for packet capture.
        */
    if (!(linktype = pcap_datalink(p))) {
            fprintf(stderr,
                    "Error getting link layer type for interface %s",
                    ifname);
            exit(9);
    }
    //printf("The link layer type for packet capture device %s is: %d.\n",
    //        ifname, linktype);



    /*
        * Close the savefile opened in pcap_dump_open().
        */
    pcap_dump_close(pd);
    /*
        * Close the packet capture device and free the memory used by the
        * packet capture descriptor.
        */     
    pcap_close(p);

















    printf("Received total: %d\n", recv_count);



    for (int i = 0; i < recv_count; i++)
    {
        fout << lengths[i] << " " <<  timestamps[i] << std::endl;
    }




    return 0;





}


void task2(int &recv_count)
{

}


void my_packet_handler( // Callback function (Only thing it does is try to record the time, and dump the packets into a dump file, in the real case we probably don't want that and want to keep the packet handler as simple as possibel)
    u_char *args,
    const struct pcap_pkthdr *header,
    const u_char *packet)
{



    //timestamps[recv_count] = header->ts.tv_sec * 1000000000 + header->ts.tv_usec * 1000;
    //lengths[recv_count] = header->len;

    //packetData2[recv_count] = *packet;

    // copy packet to packetData2
    //packetData2[recv_count*9000] = 0;
    //printf("index = %d, caplen=%d\n", recv_count, header->caplen);



    // instead of doing that, try to change only the pointers ownership
    //packetHeader[recv_count] = *header;
    //std::memcpy(&packetData2[recv_count*9000], packet, header->caplen);

    //recv_count++;

    //if ( recv_count % 10 == 0)
    //{
    //    printf("Received total: %d. Length: %d\n", recv_count, packetHeader[recv_count-3].len);
    //}


    //std::stringstream ss;
    //for(int i=0; i<header->len; ++i)
    //    ss << std::hex << (int)packet[i];
    //std::string pkt_string = ss.str();


    //fout << header->len << "  " << header->caplen << "  " << counted_time << "  " << pkt_string << std::endl;
    
    //std::cout << header->len << "  " << header->caplen << "  " << recv_count << std::endl;

    return;
}
