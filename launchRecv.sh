set -e

g++ recvPkt.cpp -o recv -O3 -lpcap
#g++ recvPkt.cpp -o recv -I/home/koto/PF_RING/userland/lib -I/home/koto/PF_RING/userland/libpcap-1.10.1/ `/home/koto/PF_RING/userland/lib/pfring_config --include`
#sudo chgrp pcap main
sudo chmod 750 recv
sudo setcap cap_net_raw,cap_net_admin=eip recv
./recv
