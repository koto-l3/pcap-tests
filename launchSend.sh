set -e

g++ sendPkt.cpp -o send -lpcap
#sudo chgrp pcap main
sudo chmod 750 send
sudo setcap cap_net_raw,cap_net_admin=eip send
./send
