#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <vector>
#include <chrono>
#include <unistd.h>
#include <pcap.h>



struct counters{
    int sent_total = 0;
};
counters c;


int packetsPerChunk = 1000;
int packetLength = 9000;
int64_t timestamp_0 = 0;





void deviceList()
{
    pcap_if_t *alldevs;
    pcap_if_t *d;
    int i = 0;
    char errbuf[PCAP_ERRBUF_SIZE];

    /* Retrieve the device list from the local machine */
    if (pcap_findalldevs(&alldevs, errbuf) == -1)
    {
        fprintf(stderr, "Error in pcap_findalldevs_ex: %s\n", errbuf);
        exit(1);
    }

    pcap_if_t *cur = alldevs;
    while (cur)
    {
        printf("Device: %s - %s\n", cur->name, cur->description ? cur->description : "");
        cur = cur->next;
    }

    /* Print the list */
    for (d = alldevs; d != NULL; d = d->next)
    {
        printf("%d. %s", ++i, d->name);
        if (d->description)
            printf(" (%s)\n", d->description);
        else
            printf(" (No description available)\n");
    }

    if (i == 0)
    {
        printf("\nNo interfaces found! sudo run me\n");
        return;
    }

    /* We don't need any more the device list. Free it */
    pcap_freealldevs(alldevs);
}


uint16_t swap(uint16_t x)
{ // swap the endianess
    uint16_t aa = (x & 0xff00) >> 8;
    uint16_t bb = ((x & 0x00ff) << 8) | aa;
    return bb;
}


int main()
{


    // OFC2 Header
    uint8_t p_packet_number, p_ofc2_number;
    uint16_t p_ofc2_evt_counter;

    int nADCs = 16*18; // 16 modules x 18 crates

    // ADC Header
    std::vector<uint8_t>  p_crate_id(nADCs), p_slot_id(nADCs);
    std::vector<uint16_t> p_spill_couner(nADCs), p_evt_counter(nADCs), p_cluster_bit(nADCs);
    std::vector<uint32_t> p_timestamp(nADCs);

    // ADC Data (16 waveforms per ADC)
    std::vector<std::vector<uint16_t>> p_waveforms(nADCs, std::vector<uint16_t>(64));

    // ADC Footer
    std::vector<uint32_t> p_crc_32(nADCs);


    int n16BitsPerEvt = 0;

    // Fill data

    p_packet_number = 0;
    p_ofc2_number = 1;
    n16BitsPerEvt += 1;

    p_ofc2_evt_counter = 2;
    n16BitsPerEvt += 1;

    for (int i = 0; i < nADCs; i++)
    {
        p_crate_id[i]     = i / 18;
        p_slot_id[i]      = 2;
        n16BitsPerEvt += 1;
        p_spill_couner[i] = 50;
        p_evt_counter[i]  = 1000;
        p_cluster_bit[i]  = 0;
        n16BitsPerEvt += 3;
        p_timestamp[i]    = 3333333;
        n16BitsPerEvt += 2;

        for (int j = 0; j < 64; j++)
        {
            p_waveforms[i][j] = j;
            n16BitsPerEvt += 1;
        }
        
        p_crc_32[i] = 9999;
        n16BitsPerEvt += 2;
    }

    printf("n16BitsPerEvt: %d\n", n16BitsPerEvt); //20738
    u_char packet[packetLength]; // Make this uchar!
    for (int i = 0; i < packetLength; i++)
    {
        packet[i] = 0xdd;
    }
     /* Supposing to be on ethernet, set mac destination to 1:1:1:1:1:1 */
    packet[0]=0xaa;
    packet[1]=0xaa;
    packet[2]=0xaa;
    packet[3]=0xaa;
    packet[4]=0xaa;
    packet[5]=0x00;

    /* set mac source to 2:2:2:2:2:2 */
    packet[6] =0xbb;
    packet[7] =0xbb;
    packet[8] =0xbb;
    packet[9] =0xbb;
    packet[10]=0xbb;
    packet[11]=0x01;

    packet[12]=17; // 8 to 15
    packet[13]=0x00;
    //packet[14]=0x00;
    //packet[15]=0x01;
    //packet[16]=0x0a;
    //packet[17]=0x01;
    //packet[18]=0x00;
    //packet[19]=0x01;
    //packet[20]=0x04;
    //packet[21]=0xd2;
    //packet[22]=0x04;
    //packet[23]=0xd2;
    //packet[24]=0x23;
    //packet[25]=0x06;
    //packet[26]=0x5f;
    //packet[27]=0xff;





        pcap_t *fp;
        char errbuf[PCAP_ERRBUF_SIZE];
        /* Open the output device */
        if ( (fp= pcap_open_live("ens7f0",            // name of the device
                            BUFSIZ,                // portion of the packet to capture (only the first 100 bytes)
                            PCAP_OPENFLAG_PROMISCUOUS,  // promiscuous mode
                            1000,               // read timeout
                            errbuf              // error buffer
                            ) ) == NULL)
        {
            printf("%s\n", errbuf);
            return -1;
        }


    //deviceList();
    //return 0;
    int64_t tag = 0;
    while (1)
    {

        //packet[12] = 4;



        if (c.sent_total % packetsPerChunk == 0)
        {
            uint64_t counted_time = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
            //const auto p1 = std::chrono::system_clock::now();
            //uint64_t counted_time = std::chrono::duration_cast<std::chrono::nanoseconds>(p1.time_since_epoch()).tag();

            double time = (counted_time - timestamp_0); // ns
            double size = packetsPerChunk * packetLength * 8; // bits

            printf("Speed: %f Gbps\n", size / time);
            timestamp_0 = counted_time;

        }

        if (c.sent_total == 1000)
        {
            printf("sent: %d packets\n", c.sent_total);
            sleep(3);
            return 0;
        }
        //if (c.sent_total % 9 == 0)
        //{
        //    tag += 1;
        //    for (int i = 12; i < packetLength; i++)
        //    {
        //        //packet[i % packetLength] ++;
        //        //if (packet[i % packetLength] > 10)
        //        //{
        //        //    packet[i % packetLength] = 0;
        //        //}
        //    }
        //}


        if (0) // Complex option. Emulate actual packets
        {

            int p_idx = 0;

            // OFC Header
            packet[p_idx++] = p_packet_number << 8 | p_ofc2_number, 
            packet[p_idx++] = p_ofc2_evt_counter;


            for (int i = 0; i < nADCs; i++)
            {

                // ADC Header
                packet[p_idx++] = p_crate_id[i] << 8 | p_slot_id[i];
                packet[p_idx++] = p_spill_couner[i];
                packet[p_idx++] = p_evt_counter[i];
                packet[p_idx++] = p_cluster_bit[i];
                packet[p_idx++] = (p_timestamp[i] & 0xff00) >> 16;
                packet[p_idx++] = (p_timestamp[i] & 0x00ff);

                // ADC Data
                for (int j = 0; j < 64; j++)
                {
                    packet[p_idx++] = p_waveforms[i][j];
                }

                // ADC Footer
                packet[p_idx++] = (p_crc_32[i] & 0xff00) >> 16;
                packet[p_idx++] = (p_crc_32[i] & 0x00ff);           
            }
        }


        //for (int i = 0; i < 20; i++)
        //{
        //    int c = packet[i] << 8 | packet[i + 1];
        //    printf("%d ", packet[i]);
        //}
        //printf("Packet size: %d\n", packetLength);


        /* Send down the packet */
        if (pcap_sendpacket(fp, packet, packetLength /* size */) != 0)
        {
            fprintf(stderr, "\nError sending the packet: \n", pcap_geterr(fp));
            return -1;
        }
        c.sent_total ++;
    }

    pcap_close(fp);
    return 0;
}





